#!/bin/bash

docker container inspect loki >/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "Loki instance already exists. Exiting"
	exit 0
fi

echo "Starting Loki instance"
docker run \
	--detach \
	--name loki \
	--publish 3100:3100 \
	--restart unless-stopped \
	grafana/loki:2.4.2 
